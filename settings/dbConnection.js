const Sequelize = require('sequelize');
const settings = require('./settings.js');

module.exports = new Sequelize(settings.DB_NAME, settings.DB_USER, settings.DB_PWD, {
	host: settings.DB_HOST,
	dialect: 'mariadb',
	dialectOptions: {
		timezone: 'Etc/GMT-5',
	},
});